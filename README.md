### Overview

The spatialQC package aims to visualise variations in sequencing QC metrics across the surfaces of a flowcell

### Installation

```S
devtools::install_bitbucket("spatialQC", "grimbough")
```






