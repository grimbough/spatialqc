addList <- function (x, val) 
{
    if(is.null(x)) {
        return(val)
    }
    stopifnot(is.list(x), is.list(val))
    if(identical(names(x), names(val))) {
        x <- mapply("+", x, val, SIMPLIFY = FALSE)
    } else {
        for (v in names(val)) {
            if (v %in% names(x)) 
                x[[v]] <- x[[v]] + val[[v]]
            else 
                x[[v]] <- val[[v]]
        }
    }
    return( x )
}

convertToProportion <- function(res, index) {
    
    countsIndex <- which(names(res) == "totalReadPairs")
    ## if we've specified to divde the totalReadPairs entry, don't!
    if(countsIndex %in% index) { index <- index[-match(countsIndex, index)] }
    
    for(i in index) {
        res[[i]] <- relist(unlist(res[[i]]) / unlist(res[[countsIndex]]), skeleton = res[[i]])
    }
    return( res )
}


createGradientLabels <- function(imageMatrix, nbreaks, vertical = FALSE) {
    
    sep <- ifelse(vertical, " -\n", " - ")
    
    labs <- levels(cut(imageMatrix, breaks = nbreaks, dig.lab = 5))
    labs <- paste(as.numeric( sub("\\((.+),.*", "\\1", labs) ), 
                  as.numeric( sub("[^,]*,([^]]*)\\]", "\\1", labs) ),
                  sep = sep)
    return(labs)
}
    